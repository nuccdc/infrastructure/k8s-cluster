{
  description = "k8s-cluster";

  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    devShell.x86_64-linux = pkgs.mkShell {
      name = "tanzu-state";
      buildInputs = with pkgs; [
        talosctl
        kubectl
        argocd
        kubernetes-helm
        yq
        jq
        velero
      ];
    };
  };
}
