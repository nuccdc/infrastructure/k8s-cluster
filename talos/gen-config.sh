#!/bin/sh
talosctl gen config proxmox-cluster https://172.24.155.2:6443 \
  --config-patch "$(cat ./both-patches.yaml)" \
  --config-patch-control-plane "$(cat ./controlplane-patches.yaml)" \
  --config-patch-worker "$(cat ./worker-patches.yaml)"
