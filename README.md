# k8s-cluster

ArgoCD state for our Talos k8s cluster.

# Setup

```
kubectl apply -k ./setup
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo
echo -ne '<gitlab_token>' > token
kubectl -n external-secrets create secret generic gitlab-secret --from-file=token
```
